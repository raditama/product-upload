module.exports = (sequelize, Sequelize) => {
    const Product = sequelize.define("product", {
        nama_barang: {
            type: Sequelize.STRING
        },
        kode: {
            type: Sequelize.STRING
        },
        jumlah_stok: {
            type: Sequelize.INTEGER
        },
        keterangan: {
            type: Sequelize.STRING
        }
    });

    return Product;
};