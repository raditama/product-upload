module.exports = {
    HOST: "localhost",
    USER: "postgres",
    PASSWORD: "12345678",
    port: "5432",
    DB: "restful_db",
    dialect: "postgres",
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
  };