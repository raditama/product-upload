const db = require("../../models");
const Product = db.product;

const readXlsxFile = require("read-excel-file/node");

const upload = async (req, res) => {
  try {
    if (req.file == undefined) {
      return res.status(400).send("Please upload an excel file!");
    }

    let path =
      __basedir + "/resources/static/assets/uploads/" + req.file.filename;

    readXlsxFile(path).then((rows) => {
      // skip header
      rows.shift();

      let products = [];

      rows.forEach((row) => {
        let product = {
          nama_barang: row[0],
          kode: row[1],
          jumlah_stok: row[2],
          keterangan: row[3]
        };

        products.push(product);
      });

      let tempProducts = [];
      tempProducts.push(products[0]);

      if (products.length > 1) {
        for (var j = 1; j < products.length; j++) {
          let same = false;
          let sameKey = 0;
          for (var i = 0; i < tempProducts.length; i++) {
            if (products[j].kode === tempProducts[i].kode) {
              same = true;
              sameKey = i;
              break;
            }
          }
          if (same) {
            // process penjumlaham jika ada data sama
            let product = {
              nama_barang: products[j].nama_barang,
              kode: products[j].kode,
              jumlah_stok: Number(products[j].jumlah_stok) + Number(tempProducts[sameKey].jumlah_stok),
              keterangan: products[j].keterangan
            };
            tempProducts[sameKey] = product;
          } else {
            tempProducts.push(products[j]);
          }
        }
      }

      Product.bulkCreate(tempProducts)
        .then(() => {
          res.status(200).send({
            message: "Uploaded the file successfully: " + req.file.originalname,
          });
        })
        .catch((error) => {
          res.status(500).send({
            message: "Fail to import data into database!",
            error: error.message,
          });
        });
    });
  } catch (error) {
    console.log(error);
    res.status(500).send({
      message: "Could not upload the file: " + req.file.originalname,
    });
  }
};

const getproduct = (req, res) => {
  Product.findAll()
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving product.",
      });
    });
};

module.exports = {
  upload,
  getproduct,
};